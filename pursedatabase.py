﻿
import pyodbc

class PurseDatabase:
	__purse_db_driver_name = '{Microsoft Access Driver (*.mdb, *.accdb)}';

	def __init__(self, purse_db_path):
		self.__purse_db_path = purse_db_path

	def execute_query(self, sql_text):
		conn = pyodbc.connect('DRIVER={};DBQ={}'.format(self.__purse_db_driver_name, self.__purse_db_path))
		cursor = conn.cursor()
		rows = cursor.execute(sql_text).fetchall();
		cursor.close()
		conn.close()
		return rows


