
import urllib.request, json 

class Moex:
	@staticmethod
	def get_bond_prev_price(board, isin):
		url = "http://moex.com/issrpc/marketdata/stock/bonds/daily/full/result.json?boardid={}&lang=ru&secid={}".format(board, isin)
		
		result = None
		try:
			response = urllib.request.urlopen(url)
			data = json.loads(response.read())
			moex_value_name = "PREVPRICE"
			if (len(data) == 2) and (moex_value_name in data[1]):
				result = data[1][moex_value_name];
		except:
			result = None;
		
		return result 
