﻿import datetime
from scipy import optimize
from datetime import date, timedelta

class Bond:
	def __init__(self, db, inv_product_id, bond_nominal = None, url = None, last_payment_date = None):
		self.Id = inv_product_id
		self.__database = db
		self.__nominal = bond_nominal
		self.Url = url
		self.__tax_rate = None
		self.__last_payment_date = last_payment_date
		self.__initial_date = None;
		self.__current_debt = None
		self.__next_amort = None
		self.__isin = None
		self.__isin_load_flag = False
		self.__short_name = None
		self.__coupons = None;
		self.__accrued_interest = None;

	def get_short_name(self):
		if self.__short_name is None:
			self.__refresh_inv_product_info();
		return self.__short_name
	
	def get_last_payment_date(self):
		if self.__last_payment_date is None:
			self.__refresh_bond_info();
		return self.__last_payment_date

	def get_initial_date(self):
		if self.__initial_date is None:
			self.__refresh_bond_info();
		return self.__initial_date

	def __refresh_bond_info(self):
		sql_text = "SELECT nominal, date_to, url, tax, date_from FROM inv_bond WHERE inv_product_id = {}".format(self.Id)
		query_result = self.__database.execute_query(sql_text)
		self.__nominal = query_result[0][0]
		self.__last_payment_date = query_result[0][1].date()
		self.Url = query_result[0][2]
		self.__tax_rate = query_result[0][3]
		self.__initial_date = query_result[0][4].date()

	def get_accrued_interest(self):
		if self.__accrued_interest is None:
			self.__calc_accrued_interest();
		return self.__accrued_interest

	def __calc_accrued_interest(self):
		today = date.today()
		coupon = self.get_bond_coupon();
		if len(coupon) == 0:
			self.__accrued_interest = 0;
		else:
			prev_coupon_date = self.get_initial_date();
			next_coupon_date = self.get_last_payment_date();
			next_coupon_amount = None;
			for c in coupon:
				coupon_date = c[0]
				if (coupon_date >= today) and (next_coupon_date >= coupon_date):
					next_coupon_date = coupon_date
					next_coupon_amount = c[1]
				if (coupon_date < today) and (prev_coupon_date < coupon_date):
					prev_coupon_date = coupon_date
			
			coupon_period_len = next_coupon_date - prev_coupon_date
			diff_to_today = today - prev_coupon_date
			self.__accrued_interest = round(next_coupon_amount*diff_to_today / coupon_period_len, 4)

	def get_tax_rate(self):
		if self.__tax_rate is None:
			self.__refresh_bond_info();
		return self.__tax_rate

	def get_bond_isin(self):
		if not self.__isin_load_flag:
			self.__refresh_inv_product_info();
			self.__isin_load_flag = True;
		return self.__isin
	
	def __refresh_inv_product_info(self):
		sql_text = "SELECT code_1, code_2, code_3, short_name FROM inv_product WHERE inv_product_id = {}".format(self.Id)
		query_result = self.__database.execute_query(sql_text)[0]
		self.__short_name = query_result[3] 
		code = query_result[0]
		if (code is not None) and (len(code) == 12):
			self.__isin = code
		else:
			code = query_result[1]
			if (code is not None) and (len(code) == 12):
				self.__isin = query_result[1]
			else:
				code = query_result[2]
				if (code is not None) and (len(code) == 12):
					self.__isin = query_result[2]
				else:
					self.__isin = None

	def get_nominal(self):
		if self.__nominal is None:
			self.__refresh_bond_info();
		return self.__nominal
	
	def get_is_cashflows_full_deterministically(self):
		coupon = self.get_bond_coupon()
		for c in coupon:
			coupon_amount = c[1]
			if (coupon_amount is None):
				return False
		return True

	def get_cashflows(self, price):
		result = list()
		
		debt = self.get_current_debt()
		result.append([date.today(), -price*debt])
		
		accrued_interest_amount = self.get_accrued_interest()
		result.append([date.today(), -accrued_interest_amount])
		
		broker_fee_rate = 0.00057
		result.append([date.today(), -debt*broker_fee_rate])
		
		coupons = self.get_bond_coupon()
		tax = self.get_tax_rate()
		today = date.today()
		for c in coupons:
			coupon_date = c[0]
			coupon_amount = c[1]
			if coupon_date >= today:
				result.append([coupon_date, coupon_amount])
				if (tax > 0):
					result.append([coupon_date, -(tax / 100)*coupon_amount])
		
		amort = self.get_next_amortizations()
		for a in amort:
			amort_date = a[0]
			amort_amount = a[1]
			result.append([amort_date, amort_amount])
			if (price < 1):
				result.append([date(amort_date.year, 12, 31), (1 - price)*amort_amount*0.13])
		
		last_payment_amount = self.get_last_payment_amount()
		if last_payment_amount != 0:
			last_payment_date = self.get_last_payment_date()
			result.append([last_payment_date, last_payment_amount])
		return result

	def __refresh_bal_info(self):
		today = date.today()
		
		amort = self.get_bond_amortizations()
		bal = self.__nominal
		last_payment = self.__nominal

		next_amort = list()
		for a in amort:
			amort_date = a[0]
			last_payment = last_payment - a[1]
			if amort_date < today:
				bal = bal - a[1]
			else:
				next_amort.append([amort_date, a[1]])
		self.__current_debt = bal
		self.__last_payment_amount = last_payment
		self.__next_amort = next_amort

	def get_next_amortizations(self):
		if self.__next_amort is None:
			self.__refresh_bal_info();
		return self.__next_amort

	def get_current_debt(self):
		if self.__current_debt is None:
			self.__refresh_bal_info();
		return self.__current_debt

	def get_last_payment_amount(self):
		if self.__last_payment_amount is None:
			self.__refresh_bal_info();
		return self.__last_payment_amount

	def get_bond_amortizations(self):
		sql_text = "SELECT dateop, amount FROM inv_bond_amortizing WHERE inv_product_id = {}".format(self.Id)
		amorts = self.__database.execute_query(sql_text)
		last_date = self.get_last_payment_date()

		for a in amorts:
			amort_date = a[0].date()
			if (amort_date <= last_date):
				yield [amort_date, a[1]]

	def get_bond_coupon(self):
		if self.__coupons is None:
			sql_text = "SELECT coupon_date, coupon_amount FROM inv_bond_coupon WHERE inv_product_id = {}".format(self.Id)
			coupouns = self.__database.execute_query(sql_text)
			last_date = self.get_last_payment_date()
			self.__coupons = list()
			for c in coupouns:
				coupon_date = c[0].date()
				coupon_amount = c[1]
				if (coupon_date <= last_date):
					self.__coupons.append([coupon_date, coupon_amount])
		return self.__coupons
