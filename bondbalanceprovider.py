import os
import csv
import time

class BondBalanceProvider:
	def __init__(self, sync_dir_path):
		self.__sync_dir_path = sync_dir_path
	
	def get_curr_russuia_bond_bal(self, client_code, account_code):
		russia_bond_isin_len = 12
		items = self.get_curr_bal(client_code, account_code, 0)
		for i in items:
			sec_code = i[0]
			if (sec_code[:2] == "RU") and (len(sec_code) == russia_bond_isin_len):
				yield i

	def get_curr_bal(self, client_code, account_code, limit_kind):
		reader = self.__get_csv_bal_info()
		for row in reader:
			client = row[0]
			account = row[1]
			sec_code = row[2]
			bal_info = int(row[3])
			qty = int(row[4])
			position_price = float(row[5])
			if (bal_info == limit_kind) and (client == client_code) and (account == account_code) and (qty != 0):
				yield (sec_code, qty, position_price)

	def __get_csv_bal_info(self):
		self.__request_refresh_bal();
		csv_file_path = self.__sync_dir_path + "dumpDepoLimit.dump"
		result = csv.reader(open(csv_file_path, 'r'), delimiter='\t');
		return result

	def __request_refresh_bal(self):
		draft_file_path = self.__sync_dir_path + "dumpDepoLimit.draft"
		if os.path.isfile(draft_file_path):
			os.remove(draft_file_path)
		
		file = open(draft_file_path, "w+")
		file.close()
		
		request_file_path = self.__sync_dir_path + "dumpDepoLimit.request"
		while os.path.isfile(request_file_path):
			time.sleep(1)
		
		os.rename(draft_file_path, request_file_path)
		
		while os.path.isfile(request_file_path):
			time.sleep(1)
