
class Operation:
	def __init__(self, id, operation_ts = None, operation_type_id = None, qty = None, price = None):
		self.__operation_types = {1:"B", 2:"S"}
		self.Id = id
		self.Operation_ts = operation_ts
		self.Operation_type_id = operation_type_id
		self.Quantity = qty
		self.Price = price
		
	def get_operation_type_code(self):
		return self.__operation_types[self.Operation_type_id]
	
	@staticmethod
	def get_operations(portfolio_id, product_id):
		db = PurseDatabase()
		sql_text = "SELECT id, dateOp, inv_operation_type_id, count, price FROM inv_portfolio_product_operation WHERE inv_portfolio_id = {} AND inv_product_id = {}".format(portfolio_id, product_id)
		query_result = db.execute_query(sql_text)
		for r in query_result:
			id = r[0]
			tnx_ts = r[1]
			inv_operation_type_id = r[2]
			qty = r[3]
			price = r[4]
			o = Operation(id, tnx_ts, inv_operation_type_id, qty, price)
			yield o

