﻿import os
import json
import math
import time

from datetime import date
from bond import Bond
from bonddatabase import BondDatabase
from pursedatabase import PurseDatabase

from moex import Moex
from bondbalanceprovider import BondBalanceProvider
from operation import Operation

def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier

def xirr(transactions):
	d0 = min(transactions, key = lambda x: x[0])[0]
	step = 0.05
	guess = 0.1
	epsilon = 0.0001
	limit = 0
	while limit < 10000:
		limit += 1
		npv = 0
		for i in transactions:
			cashflow_item_years_from_start = (i[0] - d0).days / 365.0
			npv += i[1] / pow(guess, cashflow_item_years_from_start)

		if abs(npv) > epsilon:
			if npv > 0:
				guess += step
			else:
				guess -= step
				step /= 2.0
		else:
			break;
	return guess - 1

def is_bond_ready_for_trade(bond, board):
	isin = bond.get_bond_isin()
	if (isin is None):
		return False;
	
	prev_price = Moex.get_bond_prev_price(board, isin);
	if (prev_price is None):
		return False;
	
	if (not bond.get_is_cashflows_full_deterministically()):
		return False;
	
	return True

def calc_price_by_xirr(bond, target_rate, target_rate_max_deviation, curr_avg_price = None):
	xirr_val = 0
	low_price = 0
	hi_price = 2
	i = 0
	curr_deviation = None
	curr_price = None
	while (i < 999):
		i = i + 1
		curr_price = ((hi_price - low_price) / 2.0) + low_price
		curr_cashflows = bond.get_cashflows(curr_price)
			
		if (curr_avg_price is not None) and (curr_avg_price < curr_price):
			tax_rate = 0.13
			premium = (curr_price - curr_avg_price)*bond.get_current_debt()*tax_rate
			curr_cashflows.append([date.today(), premium])
		xirr_val = xirr(curr_cashflows);
		if (xirr_val >= target_rate):
			low_price = curr_price
		if (xirr_val <= target_rate):
			hi_price = curr_price
		curr_deviation = abs(xirr_val - target_rate)
		if (curr_deviation <= target_rate_max_deviation):
			break;

	if (curr_deviation <= target_rate_max_deviation):
		return curr_price
	return None

print("Start work")

with open('config.json') as json_config_file:
	config = json.load(json_config_file)

target_dir_path = config['qlua']['target_dir_path']
board_code = config['process']['board_code']

client_code = config['process']['client_code']
account = config['process']['account']

target_rate = config['process.buy']['target_interest_rate'] / 100.0000
target_rate_max_deviation = config['process.buy']['target_rate_max_deviation'] / 100.0000
critical_price = config['process.buy']['critical_price'] / 100.0000

purse_database_path = config['purse_database']['purse_database_path'] 
max_order_amount = config['process.buy']['max_order_amount']

max_day_to_finish = config['process.buy']['max_day_to_finish']

print("Start generate buy config")

purse_database = PurseDatabase(purse_database_path)
bond_database = BondDatabase(purse_database)

if board_code == "TQOB":
	bonds = bond_database.get_gov_bonds();
else:
	bonds = bond_database.get_muni_bonds();

bond_num = 0
quik_config_text = ""

for b in bonds:
	finish_date = b.get_last_payment_date()
	duration_factor = ((finish_date - date.today()).days > max_day_to_finish)
	if (not is_bond_ready_for_trade(b, board_code)) or (duration_factor):
		continue;

	print("Start process " + b.get_short_name())
	price = calc_price_by_xirr(b, target_rate, target_rate_max_deviation)
	if (price < critical_price):
		traiding_system_price = round(100*price, 2)
		isin = b.get_bond_isin()
		current_debt_amount = b.get_current_debt()
		quantity = int(max_order_amount / ((price*current_debt_amount) + b.get_accrued_interest()))
		if (quantity > 0):
			line = "\n[{}]={{ classCode = '{}', price = {}, isin = '{}', clientCode = '{}', account = '{}', directionCode = 'B', quantity = {}, short_name = '{}'}},".format(bond_num, board_code, traiding_system_price, isin, client_code, account, quantity, b.get_short_name());
			quik_config_text = quik_config_text + line
			bond_num = bond_num + 1

if (bond_num > 0):
	quik_config_text = "local a = { " + quik_config_text + "\n}\nreturn a"
	sync_file_code = "buyBonds"
	draft_file_path = target_dir_path + sync_file_code + ".draft"
	if os.path.isfile(draft_file_path):
		os.remove(draft_file_path)
	file = open(draft_file_path, "w+")
	file.close()
	request_file_path = target_dir_path + sync_file_code + ".request"
	if os.path.isfile(request_file_path):
		print("Wait traiding system for buy")
	while os.path.isfile(request_file_path):
		time.sleep(1)
	config_file_path = target_dir_path + sync_file_code + ".config"
	config_file = open(config_file_path, "w+")
	config_file.write(quik_config_text)
	config_file.close()
	os.rename(draft_file_path, request_file_path)

print("Finish work")
quit()

