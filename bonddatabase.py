﻿
from bond import Bond
from datetime import date, timedelta


class BondDatabase:
	def __init__(self, purse_database):
		self.__purse_database = purse_database
	
	def get_bond_by_isin(self, isin):
		sql_text = "SELECT inv_product_id, short_name FROM inv_product WHERE code_1 = '{}' OR code_2 = '{}' OR code_3 = '{}'".format(isin, isin, isin)
		query_result = self.__purse_database.execute_query(sql_text)[0]
		result = Bond(self.__purse_database, query_result[0])
		result.__short_name = query_result[1]
		return result

	def __get_bond_by_query(self, sql_text):
		query_result = self.__purse_database.execute_query(sql_text)
		today = date.today()
		for r in query_result:
			close_date = r[3].date()
			if (close_date > today):
				url = r[2]
				b = Bond(self.__purse_database, r[0], r[1], url, close_date)
				b.Tax = r[4]
				yield b
		
	
	def get_bonds(self, sector, status = 4, currency = 810):
		sql_text = "SELECT inv_product_id, nominal, url, date_to, tax FROM inv_bond WHERE sector_id = {} AND status_id = {} AND idCurrency = {}".format(sector, status, currency)
		return self.__get_bond_by_query(sql_text)

	def get_muni_bonds(self):
		muni_sector_id = 3
		return self.get_bonds(muni_sector_id)

	def get_gov_bonds(self):
		sector_id = 1
		active_status_id = 4
		iso_currency_code = 810
		good_bond_name_mask = u'ОФЗ'
		sql_text = u"SELECT inv_product_id, nominal, url, date_to, tax FROM inv_bond WHERE sector_id = {} AND status_id = {} AND idCurrency = {}".format(sector_id, active_status_id, iso_currency_code)
		for b in self.__get_bond_by_query(sql_text):
			if (good_bond_name_mask in b.get_short_name().upper()):
				yield b
		
	def get_corp_bonds(self):
		corp_sector_id = 6
		return self.get_bonds(corp_sector_id)
